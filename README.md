# docker_scripts

Scripts to compile tf docker containers.

Update Dockerfile with the correct container for the OS and CUDA version
that can be found in https://hub.docker.com/r/nvidia/cuda/

Also update the tensorflow wheel with the correct one from
https://www.tensorflow.org/install/pip