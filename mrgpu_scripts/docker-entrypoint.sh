#!/bin/bash

echo "export PATH=${PATH}:/opt/conda/bin" >> ~/.bashrc

echo "export PYTHONPATH=/home/m145916/TF/tf_slim_models/research/slim/" >> ~/.bashrc

exec "$@"
