#!/bin/bash

nvidia-docker run --runtime=nvidia --privileged=true -v /Volumes/eplab:/Volumes/eplab -v /Volumes/xltekarc:/Volumes/xltekarc -v /Volumes/xltekarchives:/Volumes/xltekarchives -v /mnt/Hydrogen/yoga:/mnt/Hydrogen/yoga -v /home/yoga:/home/yoga -p 7777:7777 -p 6006:6006 -p 6007:6007 -p 6008:6008 -p 6009:6009 -it theesan16/tf:msel_yoga
